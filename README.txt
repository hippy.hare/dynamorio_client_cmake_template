
BUILD:
cmake -B build -G Ninja
cmake --build build

RUN ON STARTUP:
./build/_deps/dynamorio-src/bin64/drrun -v -c ./build/dr_tempalate_client.dll -- notepad

RUN WITH ATTACH:
./build/_deps/dynamorio-src/bin64/drrun -v -attach 1234 -c ./build/dr_tempalate_client.dll


TEST ATTACH:
cmake --build build --clean-first ; Start-Process -FilePath "mspaint.exe"; Start-Sleep -Seconds 1; .\build\_deps\dynamorio-src\bin64\drrun.exe -v -attach $(Get-Process mspaint | Select-Object -ExpandProperty Id) -c .\build\dr_tempalate_client.dll