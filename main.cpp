#define DR_FAST_IR

#include "dr_api.h"
#include "dr_tools.h"
#include "lib/utils.h"
// #include <drmgr.h>

file_t global_log;


static void event_exit(void) { /* empty client */
  dr_fprintf(global_log, "done.");
  log_file_close(global_log);
}

DR_EXPORT void dr_client_main(client_id_t id, int argc, const char *argv[]) {

  global_log =
      log_file_open(id, NULL, NULL, get_timestamp_string(), NULL);
  dr_register_exit_event(event_exit);
}
