#include "utils.h"
#include "dr_tools.h"
#include "drx.h"

file_t log_file_open(client_id_t id, void *drcontext, const char *path,
                     const char *name, uint flags) {
  file_t log;
  char log_dir[MAXIMUM_PATH];
  char buf[MAXIMUM_PATH];
  size_t len;
  char *dirsep;

  DR_ASSERT(name != NULL);
  len = dr_snprintf(log_dir, BUFFER_SIZE_ELEMENTS(log_dir), "%s",
                    path == NULL ? dr_get_client_path(id) : path);
  DR_ASSERT(len > 0);
  NULL_TERMINATE_BUFFER(log_dir);
  dirsep = log_dir + len - 1;
  if (path == NULL /* removing client lib */ ||
      /* path does not have a trailing / and is too large to add it */
      (*dirsep != '/' IF_WINDOWS(&&*dirsep != '\\') &&
       len == BUFFER_SIZE_ELEMENTS(log_dir) - 1)) {
    for (dirsep = log_dir + len; *dirsep != '/' IF_WINDOWS(&&*dirsep != '\\');
         dirsep--)
      DR_ASSERT(dirsep > log_dir);
  }
  /* remove trailing / if necessary */
  if (*dirsep == '/' IF_WINDOWS(|| *dirsep == '\\'))
    *dirsep = 0;
  else if (sizeof(log_dir) > (dirsep + 1 - log_dir) / sizeof(log_dir[0]))
    *(dirsep + 1) = 0;
  NULL_TERMINATE_BUFFER(log_dir);
  /* we do not need call drx_init before using drx_open_unique_appid_file */
  log = drx_open_unique_appid_file(log_dir, dr_get_process_id(), name, "log",
                                   flags, buf, BUFFER_SIZE_ELEMENTS(buf));
  if (log != INVALID_FILE) {
    char msg[MAXIMUM_PATH];
    len = dr_snprintf(msg, BUFFER_SIZE_ELEMENTS(msg), "Data file %s created",
                      buf);
    DR_ASSERT(len > 0);
    NULL_TERMINATE_BUFFER(msg);
    dr_log(drcontext, DR_LOG_ALL, 1, "%s", msg);
#ifdef SHOW_RESULTS
    DISPLAY_STRING(msg);
#ifdef WINDOWS
    if (dr_is_notify_on()) {
      /* assuming dr_enable_console_printing() is called in the initialization
       */
      dr_fprintf(STDERR, "%s\n", msg);
    }
#endif /* WINDOWS */
#endif /* SHOW_RESULTS */
  }
  return log;
}

void log_file_close(file_t log) { dr_close_file(log); }

char *get_timestamp_string() {
  dr_time_t timestamp;
  dr_get_time(&timestamp);
  // char *time_string = (char*)malloc(20);
  char *time_string = (char *)dr_global_alloc(20);
  dr_snprintf(time_string, 20, "%d-%d-%d-%d-%d-%d", timestamp.year,
              timestamp.month, timestamp.day, timestamp.hour, timestamp.minute,
              timestamp.second);
  return time_string;
}
